<?php

namespace App\Exports;

use App\Models\Response;
use Maatwebsite\Excel\Concerns\FromCollection;

class ResponseExport implements FromCollection
{
    public function __construct($id) {
      $this->formId = $id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $responses = Response::where('form_id', $this->formId)->get();

        $newResponse = [];
        foreach ($responses as $singleResponse) {
          $newResponse[] = $singleResponse->response_value;
        }
    
        return collect($newResponse);
    }
}
