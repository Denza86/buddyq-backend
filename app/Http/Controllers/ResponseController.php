<?php

namespace App\Http\Controllers;

use App\Models\Response;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ResponseExport;
use App\Models\Form;
use App\Models\Wallet;
use App\User;
use App\Models\WalletHistory;

class ResponseController extends Controller
{
  public function submitResponse(Request $request, $formId)
  {
    //request user siapa yg isi
    $reqUser = $request->user();
    $form = Form::find($formId);

    $isExist = Response::where('user_id', $reqUser->id)->count();
    // if ($isExist) {
    //   return [
    //     "error" => true,
    //     "message" => "sudah pernah response",
    //   ];
    // }

    //limitation per criteria(job, gender, domicile, min/max_age, limit_sumRespondent)
    if ($form->criteria->job) {
      if ($reqUser->job != $form->criteria->job) {
        return [
          "error" => true,
          "message" => "job tidak sesuai",
        ];
      }
    }
    if ($form->criteria->gender) {
      if ($reqUser->gender != $form->criteria->gender) {
        return [
          "error" => true,
          "message" => "gender tidak sesuai",
        ];
      }
    }
    if ($form->criteria->domicile) {
      if ($reqUser->domicile != $form->criteria->domicile) {
        return [
          "error" => true,
          "message" => "domicile tidak sesuai",
        ];
      }
    }
    if ($form->criteria->minimum_age) {
      $userAge = explode("-", $reqUser->birth_date);
      if (
        $form->criteria->minimum_age >= (int) date('Y') - (int) $userAge[0] ||
        $form->criteria->maximum_age <= (int) date('Y') - (int) $userAge[0]
      ) {
        return [
          "error" => true,
          "message" => "umur tidak sesuai",
        ];
      }
    }

    if ($form->criteria->limit) {
      $countResponse = Response::where('form_id', $formId)->count();
      if ($countResponse == $form->criteria->limit) {
        return [
          "error" => true,
          "message" => "sudah melebihi batas response",
        ];
      }
    }


    $user = User::where('id', $reqUser->id)->with('wallet')->first();

    //bikin wallet user
    $response = Response::create([
      'form_id' =>  $formId,
      'response_value' => json_encode($request->response_value),
      'user_id' => $user->id
    ]);

    //nambahin point ke balance wallet ketika user isi form
    Wallet::find($user->wallet->id)->update([
      "balance" => $form->point + $user->wallet->balance
    ]);

    //bikin log transaksi user
    WalletHistory::create([
      "response_id" => $response->id,
      "wallet_id" => $user->wallet->id,
      "amount" => $form->point,
    ]);

    return [
      "message" => "berhasil response",
      "data" => $response
    ];
  }

  //ambil 1 response untuk di show
  public function getSingleResponse($id)
  {
    $response = Response::find($id);
    return $response;
  }

  //update response
  public function update(Request $request, $id)
  {
    Response::find($id)->update([
      'response_value' => json_encode($request->response_value),
    ]);
    $response = Response::find($id);

    return [
      "message" => "update berhasil",
      "data" => $response
    ];
  }

  //download response
  public function downloadResponse($formId)
  {
    return Excel::download(new ResponseExport($formId), 'response.xlsx');
  }

  //ambil all response
  public function getAllResponse($formId)
  {
    $responses = Response::where('form_id', $formId)->get();

    $newResponse = [];
    foreach ($responses as $singleResponse) {
      $newResponse[] = $singleResponse->response_value;
    }

    return $newResponse;
  }
}
