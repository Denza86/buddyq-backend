<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

  public function getProfile(Request $request)
  {
    return $request->user();
  }

  public function login(Request $request)
  {
    $credentials = request(['username', 'password']);
    if (!Auth::attempt($credentials)) {
      return 'username atau password salah';
    }

    $user = $request->user();
    $token = $user->createToken('token')->accessToken;

    return [
      'message' => 'berhasil login',
      'token' => $token
    ];
  }

  public function register(Request $request)
  {
    $user = User::create([
      "name" => $request->name,
      "username" => $request->username,
      "role" => "user",
      "password" => bcrypt($request->password)
    ]);

    Wallet::create([
      "balance" => 0,
      "user_id" => $user->id
    ]);

    return [
      "message" => 'register berhasil',
      "data" => $user
    ];
  }

  public function update($id)
  {
    return 'update';
  }

  public function show($id)
  {
    return "show $id";
  }

  public function delete($id)
  {
    return 'delete';
  }
}
