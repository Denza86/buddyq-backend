<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\FormInput;
use Illuminate\Http\Request;

class FormController extends Controller
{

  //bikin form
  public function create(Request $request)
  {
    $user = $request->user();
    $form = Form::create([
      'title' =>  $request->title,
      'point' =>  $request->point,
      'image_url' => $request->image_url,
      'background_color' => $request->background_color,
      'template_id' => $request->template_id,
      'user_id' => $user->id
    ]);

    foreach ($request->inputs as $value) {
      FormInput::create([
        'key' => $value['key'],
        'question' => $value['question'],
        'required' => $value['required'],
        'input_type' => $value['type'],
        'option' => json_encode($value['option']),
        'default_value' => $value['default_value'],
        'form_id' => $form->id
      ]);
    }

    return [
      "message" => "berhasil membuat form",
      "data" => $form
    ];
  }

  public function show($id)
  {
    $form = Form::where('id', $id)->with(['template', 'formInputs'])->first();
    if ($form) {
      return $form;
    }
    return 'gaada data';
  }

  public function getAll()
  {
    $form = Form::all();
    return $form;
  }

  public function update(Request $request, $id)
  {
    Form::find($id)->update([
      'title' =>  $request->title,
      'point' =>  $request->point,
      'image_url' => $request->image_url,
      'background_color' => $request->background_color,
    ]);
    $form = Form::find($id);
    
    // ini update input
    foreach ($request->updated_inputs as $value) {
      FormInput::find($value['id'])->update([
        'key' => $value['key'],
        'question' => $value['question'],
        'required' => $value['required'],
        'input_type' => $value['type'],
        'option' => json_encode($value['option']),
        'default_value' => $value['default_value'],
      ]);
    }

    // delete
    FormInput::destroy($request->deleted_inputs);

    // create new
    foreach ($request->new_inputs as $value) {
      FormInput::create([
        'key' => $value['key'],
        'question' => $value['question'],
        'required' => $value['required'],
        'input_type' => $value['type'],
        'option' => json_encode($value['option']),
        'default_value' => $value['default_value'],
        'form_id' => $form->id
      ]);
    }

    return [
      "message" => "update success"
    ];
  }

  public function delete($id)
  {
    $form = Form::findOrFail($id);
    $form->delete();
    return 'delete success';
  }
}
