<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Template;
use App\Models\TemplateInput;
use App\User;
use Illuminate\Http\Request;

class TemplateController extends Controller
{

  public function getall()
  {
    $templates = Template::all();
    return $templates;
  }

  public function create(Request $request)
  {
    $template = Template::create([
      'name' =>  $request->name,
      'image_url' => $request->image_url,
      'background_color' => $request->background_color,
    ]);

    foreach ($request->inputs as $value) {
      TemplateInput::create([
        'key' => $value['key'],
        'question' => $value['question'],
        'required' => $value['required'],
        'input_type' => $value['type'],
        'option' => json_encode($value['option']),
        'default_value' => $value['default_value'],
        'template_id' => $template->id
      ]);
    }

    return [
      "message" => "berhasil membuat template",
      "data" => $template
    ];
  }

  public function update(Request $request, $id)
  {
    $template = Template::find($id);
    $template->name = $request->name;
    $template->image_url = $request->image_url;
    $template->background_color = $request->background_color;
    $template->save();

    return [
      "message" => "update success"
    ];
  }

  public function show($id)
  {
    $template = Template::where('id', $id)->with('templateInputs')->first();
    return $template;
  }

  public function delete($id)
  {
    $template = Template::findOrFail($id);
    $template->delete();
    return 'detete success';
  }
}
