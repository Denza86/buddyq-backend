<?php

namespace App\Http\Controllers;

use App\Models\Voucher;
use App\Models\Wallet;
use App\Models\WalletHistory;
use App\User;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
  public function exchange(Request $request, $id)
  {
    $reqUser = $request->user();
    $user = User::where('id', $reqUser->id)->with('wallet')->first();

    $voucher = Voucher::find($id);
    Wallet::find($user->wallet->id)->update([
      "balance" => $user->wallet->balance - $voucher->price
    ]);

    WalletHistory::create([
      "voucher_id" => $voucher->id,
      "wallet_id" => $user->wallet->id,
      "amount" => $voucher->price * -1,
    ]);

    return [
      "message" => "berhasil exhange",
      "data" => $voucher
    ];
  }

  // CRUD 
  public function getall()
  {
    $vouchers = Voucher::all();
    return $vouchers;
  }

  public function create(Request $request)
  {
    $voucher = Voucher::create([
      'name' =>  $request->name,
      'price' => $request->price,
      'expiry_date' => $request->expiry_date,
      'terms' => $request->terms,
      'image_url' => $request->image_url,
      'code' => $request->code,
      'using_step' => $request->using_step,
    ]);

    return $voucher;

    return 'create success';
  }

  public function update(Request $request, $id)
  {
    $voucher = Voucher::find($id);
    $voucher->name = $request->name;
    $voucher->price = $request->price;
    $voucher->image_url = $request->image_url;
    $voucher->expiry_date = $request->expiry_date;
    $voucher->terms = $request->terms;
    $voucher->code = $request->code;
    $voucher->using_step = $request->using_step;
    $voucher->save();

    return [
      "message" => "update success"
    ];
  }

  public function show($id)
  {
    $voucher = Voucher::where('id', $id)->first();
    return $voucher;
  }

  public function delete($id)
  {
    $voucher = Voucher::findOrFail($id);
    $voucher->delete();
    return 'detete success';
  }
}
