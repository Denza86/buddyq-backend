<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletHistory extends Model
{
  protected $guarded = [];
  
  public function walletHistories()
  {
      return $this->hasOne(Response::class);
  }

  public function vouchers()
  {
      return $this->hasMany(Voucher::class);
  }
}
