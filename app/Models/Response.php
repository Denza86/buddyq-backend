<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
  protected $guarded = [];

  //Has
  public function walletHistory()
  {
    return $this->hasOne(WalletHistory::class);
  }

  //belongsTo
  public function form()
  {
    return $this->belongsTo(Form::class);
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function getResponseValueAttribute($value)
  {
    return json_decode($value);
  }
}
