<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormCriteria extends Model
{
  protected $guarded = [];

  public function form()
  {
    return $this->hasOne(Form::class);
  }
}
