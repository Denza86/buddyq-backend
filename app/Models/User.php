<?php

namespace App;

use App\Models\Wallet;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
  use HasApiTokens, Notifiable;

  protected $guarded = [];

  protected $hidden = ['password'];

  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  public function wallet()
  {
    return $this->hasOne(Wallet::class);
  }
}
