<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    // protected $table = 'my_flights';
    // protected $primaryKey = 'template_id';

    public function templateInputs()
    {
      return $this->hasMany(TemplateInput::class);
    }

    public function forms()
    {
      return $this->hasMany(Form::class);
    }
}