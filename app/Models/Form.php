<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends Model
{
  protected $guarded = [];
  use SoftDeletes;

  //hasMany
  public function formInputs()
  {
    return $this->hasMany(FormInput::class);
  }

  public function criteria()
  {
    return $this->hasOne(FormCriteria::class);
  }
  public function responses()
  {
    return $this->hasMany(Response::class);
  }

  //belongsTo
  public function template()
  {
    return $this->belongsTo(Template::class);
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
