<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplateInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_inputs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('question');
            $table->string('key');
            $table->boolean('required');
            $table->string('option')->nullable();
            $table->string('default_value')->nullable();
            $table->string('input_type');
            $table->timestamps();
            $table->softDeletes();

            $table->bigInteger('template_id')->unsigned();
            // $table->foreign('template_id')->references('id')->on('templates')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_inputs');
    }
}
