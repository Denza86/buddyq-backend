<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('forms', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title');
      $table->integer('point');
      $table->string('background_color');
      $table->string('image_url');
      $table->timestamps();
      $table->softDeletes();

      $table->bigInteger('user_id')->unsigned();
      $table->bigInteger('template_id')->unsigned()->nullable();

      // $table->foreign('userId')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
      // $table->foreign('templateId')->references('id')->on('templates')->onUpdate('cascade')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('forms');
  }
}
