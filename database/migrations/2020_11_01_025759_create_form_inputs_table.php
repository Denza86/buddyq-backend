<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_inputs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('question');
            $table->string('key');
            $table->boolean('required');
            $table->string('option')->nullable();
            $table->boolean('default_value')->nullable();
            $table->string('input_type');
            $table->timestamps();

            $table->bigInteger('form_id')->unsigned();

            // $table->foreign('formId')->references('id')->on('forms')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_inputs');
    }
}
