<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletHistoriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('wallet_histories', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('amount');
      $table->timestamps();
      $table->bigInteger('wallet_id')->unsigned();
      $table->bigInteger('response_id')->unsigned()->nullable();
      $table->bigInteger('voucher_id')->unsigned()->nullable();


      // $table->foreign('walletId')->references('id')->on('wallets')->onUpdate('cascade')->onDelete('cascade');
      // $table->foreign('responseId')->references('id')->on('responses')->onUpdate('cascade')->onDelete('cascade');
      // $table->foreign('voucherId')->references('id')->on('vouchers')->onUpdate('cascade')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('wallet_histories');
  }
}
