<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('wallets', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('balance');
      $table->timestamps();
      $table->bigInteger('user_id')->unsigned();

      // $table->foreign('walletId')->references('id')->on('wallets')->onUpdate('cascade')->onDelete('cascade');
      // $table->foreign('userId')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('wallets');
  }
}
