<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormCriteriaTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('form_criterias', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('limit')->nullable();
      $table->string('domicile')->nullable();
      $table->integer('minimum_age')->nullable();
      $table->integer('maximum_age')->nullable();
      $table->string('job')->nullable();
      $table->string('gender')->nullable();
      $table->timestamps();
      $table->bigInteger('form_id')->unsigned();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('form_criterias');
  }
}
