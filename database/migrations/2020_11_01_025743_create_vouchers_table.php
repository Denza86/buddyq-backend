<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('vouchers', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name');
      $table->string('image_url');
      $table->text('terms');
      $table->integer('price');
      $table->string('code');
      $table->date('expiry_date');
      $table->text('using_step');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('vouchers');
  }
}
