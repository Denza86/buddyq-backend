<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResponsesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('responses', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('response_value');
      $table->bigInteger('user_id')->unsigned();
      $table->bigInteger('form_id')->unsigned();
      $table->timestamps();

      // $table->foreign('userId')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
      // $table->foreign('formId')->references('id')->on('forms')->onUpdate('cascade')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('responses');
  }
}
