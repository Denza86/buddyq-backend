<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/user/register', 'UserController@register');
Route::post('/user/login', 'UserController@login');

Route::get('/form/{id}/response/download', 'ResponseController@downloadResponse');

// Route::group(['middleware' => 'auth:api'], function () {
  Route::get('/user/profile', 'UserController@getProfile');

  // form
  Route::get('/form', 'FormController@getall');
  Route::post('/form', 'FormController@create');
  Route::get('/form/{id}', 'FormController@show');
  Route::put('/form/{id}', 'FormController@update');
  Route::delete('/form/{id}', 'FormController@delete');

  // response
  Route::post('/form/{formId}/response', 'ResponseController@submitResponse');
  Route::get('response/{id}', 'ResponseController@getSingleResponse');
  Route::put('response/{id}', 'ResponseController@update');
  Route::get('/form/{formId}/response', 'ResponseController@getAllResponse');

  // template
  Route::get('/template', 'TemplateController@getall');
  Route::post('/template', 'TemplateController@create');
  Route::get('/template/{id}', 'TemplateController@show');
  Route::put('/template/{id}', 'TemplateController@update');
  Route::delete('/template/{id}', 'TemplateController@delete');

  // voucher
  Route::get('/voucher', 'VoucherController@getall');
  Route::post('/voucher', 'VoucherController@create');
  Route::get('/voucher/{id}/exchange', 'VoucherController@exchange');
  Route::get('/voucher/{id}', 'VoucherController@show');
  Route::put('/voucher/{id}', 'VoucherController@update');
  Route::delete('/voucher/{id}', 'VoucherController@delete');
// });
